<p align="center">
    <a href="https://gitee.com/changwenpeng/svipbot" target="_blank" rel="noopener noreferrer">
        <img src="https://q.qlogo.cn/headimg_dl?dst_uin=201088830&spec=5" alt="logo" width="150px"/>
    </a>
</p>
<h1 align="center">svipbot-demo</h1>

<p align="center">
    <a href="https://www.java.com/zh-CN/">
        <img alt="Java8" class="no-zoom" src="https://img.shields.io/badge/Java-8+-green?logo=java&logoColor=white">
    </a>
    <a href="https://github.com/spring-projects/spring-boot/tree/v2.7.7">
        <img alt="SpringBoot2.7.7" class="no-zoom" src="https://img.shields.io/badge/SpringBoot-2.7.7-blue?logo=spring&logoColor=white">
    </a>
</p>

> 💾 **[svipbot](https://gitee.com/changwenpeng/svipbot)** 是一个基于<img src="https://github.com/Mrs4s/go-cqhttp/raw/master/winres/icon.png" width="20" height="20" alt="go-cqhttp" style="max-width: 100%; line-height:20px">[go-cqhttp](https://docs.go-cqhttp.org/)的Java语言SpringBoot框架开发的QQ机器人框架SDK。
>
> - 🔁 项目同步维护：[Gitee](https://gitee.com/changwenpeng/svipbot)

### 本项目基于[SpringBoot 2.7.7](https://github.com/spring-projects/spring-boot/tree/v2.7.7)版本开发

## 1. 插件开发
### 1.1 创建一个Spring boot项目 并且导入 [`Maven`](https://maven.apache.org) 依赖项
```xml
<dependency>
    <groupId>cn.svipbot</groupId>
    <artifactId>spring-boot-starter-svipbot</artifactId>
    <version>0.0.13</version>
</dependency>
```

### 1.2 编写插件

### 1.2.1 编写示例

#### [`Java`](https://www.java.com) 示例
```java
package com.example.demo.plugins;

import cn.svipbot.gocq.bot.Bot;
import cn.svipbot.gocq.bot.BotPlugin;
import cn.svipbot.gocq.event.message.GroupMessageEvent;
import cn.svipbot.gocq.event.message.PrivateMessageEvent;
import cn.svipbot.gocq.utils.CqMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 示例插件
 * 1. 插件继承自 BotPlugin
 * 2. 添加 @Component 注解
 * 3. 添加lombok的@Slf4j打印日志
 */
@Slf4j
@Component
public class TestPlugin extends BotPlugin {

    /**
     * 收到私聊消息时会调用这个方法
     *
     * @param bot   机器人对象，用于调用API，例如发送私聊消息 sendPrivateMsg
     * @param event 事件对象，用于获取消息内容、群号、发送者QQ等
     * @return 是否继续调用下一个插件, `MatchedAndBlock` 表示不继续, `NotMatch` 表示继续
     */
    @Override
    public int onPrivateMessage(Bot bot, PrivateMessageEvent event) {
        // 获取 发送者QQ 和 消息内容
        long userId = event.getUserId();
        // 控制台打印
        log.info("私聊消息{}", event.getMessage());
        // 发送消息
        CqMsg cqMsg = new CqMsg();
        cqMsg.text("测试私聊");
        bot.sendPrivateMsg(userId, cqMsg.toString(), false);
        // 继续执行下一个插件
        return NotMatch;
    }

    /**
     * 收到群消息时调用此方法
     *
     * @param bot   机器人对象
     * @param event 事件内容
     * @return 是否继续调用下一个插件, `MatchedAndBlock` 表示不继续, `NotMatch` 表示继续
     */
    @Override
    public int onGroupMessage(Bot bot, GroupMessageEvent event) {
        // 获取 发送者QQ 和 消息内容
        long userId = event.getUserId();
        // 获取 发送消息的群号
        long groupId = event.getGroupId();
        // 控制台打印
        log.info("群消息{}", event.getMessage());
        // 发送消息
        CqMsg cqMsg = new CqMsg();
        cqMsg.at(userId).text("测试群聊");
        bot.sendGroupMsg(groupId, cqMsg.toString());
        // 继续执行下一个插件
        return NotMatch;
    }
}
```
### 1.2.2 修改配置文件

修改 resource/application.yml
```yaml
server:
  port: 8080

svipbot:
    # 请将编写的插件全类名添加在这里
    # 在收到消息时会按顺序依次调用以下插件
    # 如果前面的插件返回 MatchedAndBlock 则不会继续调用后续插件
    # 如果前面的插件返回 NotMatch 则会继续调用后续插件
    plugin-list:
      - com.example.demo.plugins.TestPlugin
```