package com.example.demo.plugins;

import cn.svipbot.gocq.bot.Bot;
import cn.svipbot.gocq.bot.BotPlugin;
import cn.svipbot.gocq.event.message.GroupMessageEvent;
import cn.svipbot.gocq.event.message.PrivateMessageEvent;
import cn.svipbot.gocq.utils.CqMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 示例插件
 * 1. 插件继承自 BotPlugin
 * 2. 添加 @Component 注解
 * 3. 添加lombok的@Slf4j打印日志
 */
@Slf4j
@Component
public class TestPlugin extends BotPlugin {

    /**
     * 收到私聊消息时会调用这个方法
     *
     * @param bot   机器人对象，用于调用API，例如发送私聊消息 sendPrivateMsg
     * @param event 事件对象，用于获取消息内容、群号、发送者QQ等
     * @return 是否继续调用下一个插件, `MatchedAndBlock` 表示不继续, `NotMatch` 表示继续
     */
    @Override
    public int onPrivateMessage(Bot bot, PrivateMessageEvent event) {
        // 获取 发送者QQ 和 消息内容
        long userId = event.getUserId();
        // 控制台打印
        log.info("私聊消息{}", event.getMessage());
        // 发送消息
        CqMsg cqMsg = new CqMsg();
        cqMsg.text("测试私聊");
        bot.sendPrivateMsg(userId, cqMsg.toString(), false);
        // 继续执行下一个插件
        return NotMatch;
    }

    /**
     * 收到群消息时调用此方法
     *
     * @param bot   机器人对象
     * @param event 事件内容
     * @return 是否继续调用下一个插件, `MatchedAndBlock` 表示不继续, `NotMatch` 表示继续
     */
    @Override
    public int onGroupMessage(Bot bot, GroupMessageEvent event) {
        // 获取 发送者QQ 和 消息内容
        long userId = event.getUserId();
        // 获取 发送消息的群号
        long groupId = event.getGroupId();
        // 控制台打印
        log.info("群消息{}", event.getMessage());
        // 发送消息
        CqMsg cqMsg = new CqMsg();
        cqMsg.at(userId).text("测试群聊");
        bot.sendGroupMsg(groupId, cqMsg.toString());
        // 继续执行下一个插件
        return NotMatch;
    }
}